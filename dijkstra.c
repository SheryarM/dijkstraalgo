#include <stdio.h>
#include <stdlib.h>
#define INF 1000000000

int vertices[7][7] = {{0, 0, 1, 1, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 0},
            {1, 1, 0, 1, 1, 0, 0},
            {1, 0, 1, 0, 0, 0, 1},
            {0, 0, 1, 0, 0, 1, 0},
            {0, 1, 0, 0, 1, 0, 1},
            {0, 0, 0, 1, 0, 1, 0}};

int edges[7][7] = {{0, 0, 1, 2, 0, 0, 0},
         {0, 0, 2, 0, 0, 3, 0},
         {1, 2, 0, 1, 3, 0, 0},
         {2, 0, 1, 0, 0, 0, 1},
         {0, 0, 3, 0, 0, 2, 0},
         {0, 3, 0, 0, 2, 0, 1},
         {0, 0, 0, 1, 0, 1, 0}};

/* keeps track of distance from the starting node */
int distance[7] = {INF, INF, INF, INF, INF, INF, INF};

/* keeps track of visited nodes */
int visited[7] = {0, 0, 0, 0, 0, 0, 0};

/* keeps track of previous node */
int prev[7] = {0, 0, 0, 0, 0, 0, 0};

/* choose the next best node */
int next_to_visit(int *distance, int *visited, int start);

int main()
{
    int count = 0;
    int start = 4; /* start node */
    int node_p = start;
    int node = start;
    distance[node] = 0;

    while(1){
        for(int i=0; i<7; i++){
	    /* ignore undirected and visited 
	    nodes from the current node   */
            if(!edges[node][i] || visited[i]){
                    continue;
            }
	    /* calculate the distance and update
	    if we found a short distance      */ 
            int dis = distance[node] + edges[node][i];
            if(dis < distance[i]){
                distance[i] = dis;
                prev[i] = node;
            }
        }
        node = next_to_visit(distance, visited, start);
        visited[node_p] = 1; /* mark the current as visisted */
        node_p = node;
        count++;
        if(count == 100){break;}
    }
    printf("DISTANCE from starting node to node \n");
    for(int i=0; i<7; i++){
        printf("%d to %d -> dis=%d \n", start, i, distance[i]);
    }printf("\n");
    /*printf("PREVIOUS \n");
    for(int i=0; i<7; i++){
        printf("%d ", prev[i]);
    }*/

    return 0;
}

int next_to_visit(int *distance, int *visited, int start){
    int dis_min = 100000;
    int min=0;
    for(int i=0; i<7; i++){
        if(visited[i] || distance[i] == INF || i == start){
            continue;
        }
	/* find the next best node based on the shortest distance */
        if(distance[i] <= dis_min){
            dis_min = distance[i];
            min = i;
        }
    }
    return min;
}
